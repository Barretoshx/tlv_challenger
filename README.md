![Build Status](https://site.tlv.ag/wp-content/uploads/2022/01/logo_completo2.png)

---
Bem vindo a TLV Challenger
Uma estrutura de gerencimento de usuarios, paginas e mais algumas coisas para ajudar na produtividade.
O modelo é simples, com seu próprio framework e pode ser utilizado com absolutamente qualquer coisa.

Lista de desafios
1 - Fazer um CRUD basico para cadastro dos Ganhadores de um ingresso, contendo Nome, Email, Telefone e CPF e Categoria(Vip, Corporate, Premium)
A forma que você irá fazer fica a sua escolha ( se são entre paginas, modais e ajax). Será avaliado a performance do codigo, cuidados com segurança nos inputs, componentes e modelagem de dados, além das soluções mais criativas
2 - Ao Clicar na pagina Minha Conta (users/accounts.php) e depois na users/user_settings.php, não conseguimos alterar nossa imagem
Só aparece a imagem se você utlizar uma email cadastrado no Gravatar.com. O programador nao entendeu isso e precisamos corrigir! Devemos ter a possibilidade de fazer um upload e mostrar nossa foto. Deve ter até um campo no banco pra isso.
Só tome cuidado coma segurança, lembre-se disso.
3 - Criar um Dashboard para o cadastro de ganhadores
Nesse dashboard devemos ter um topo com os seguintes indicadores : Total por Vip, Total Premium e Total Corporate. Logo abaixo uma listagem dessa tabela, obadecendo o clique desses indicadores no topo : Clicou em VIP, deve-se mostrar a listagem apenas dos VIP.
A aginação é impotante mas não urgente, ok? Ouvimos dizer que existe um tal de datatable.net que ajuda, mas enfim... Faça do jeito mais rapido e bonito! O cliente pediu de ultima hora.
4 - Consuma uma API REST
Aqui precisamos que você consuma uma API REST pubica, qualquer uma que achar. Pode ser por JS, pode ser via cUrl, tanto faz
Use a criatividade e apresente algo legal pra gente!

Dicas e instruções
 Após a realização de cada desafio você pode usar a classe list-group-item-success para marcar a linha como feita, e ajuda-lo na organização, exemplo:

Item Não Finalizado
Item Finalizado

 Todas essas paginas já estão criadas, bastanto clicar no link do desafio. Só implementar, mas pra isso vocÊ precisa estar logado. Use as credenciais :

- login : tripulante
- senha : #TLV@2023@@

 Todas as paginas ja estão conectadas ao equema de banco de dados.

Para fazer um select utilize instrução :
$users = $db->query("SELECT username FROM users");
Ele vai trazer um array $users e você pode iterar com o foreach ou for
Para fazer um update, pode utilizar o metodo query ou utilize instrução :
$result = $db->update("tabela",ID_do_registro, ['Nome do campo' => 'Valor']);
Ele vai responder com a quantidade de itens afetados no banco
Para fazer um insert, pode utilizar o metodo query ou utilize instrução :
$result = $db->insert("tabela", ['Nome do campo' => 'Valor']);
Ele vai responder com a quantidade de itens afetados no banco
Para fazer um delete, pode utilizar o metodo query ou utilize instrução :
$result = $db->delete("tabela", id_do_registro);
Ele vai responder com a quantidade de itens afetados no banco
Dica : Acesse users/classes/DB.php para ter acesso a todos os métodos dessa abstração.

